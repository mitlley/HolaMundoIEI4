package inacap.test.holamundoiei4d.vista;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import inacap.test.holamundoiei4d.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
